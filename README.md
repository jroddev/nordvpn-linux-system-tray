
#NordVPN Status Tray Icon

** This project was not created by NordVPN **

A small service that displays the nordvpn connection status in the system tray.  
Tested on KDE Plasma.

##Requirements
- requires qt5
- kde plasma

##Installation
- ./install.sh

Icons are stored in /usr/share/pixmaps/
Executable is stored in /usr/sbin/
Auto start-up is done via user based systemd 

##Uninstall
- ./uninstall.sh

